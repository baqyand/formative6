package com.formative1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Bogor extends Thread {

    String hurufDepan = "F";
    String belakang;
    String fileName = "Bogor.txt";
    String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    int nomor;

    @Override
    public void run() {
        try {
            FileWriter fileBaru = new FileWriter(fileName);

            for (int i = 1; i <= 10000; i++) {
                nomor = i;
                for (int j = 0; j < alphabet.length(); j++) {
                    for (int k = 0; k < alphabet.length(); k++) {
                        belakang = String.valueOf(alphabet.charAt(j)) + alphabet.charAt(k);
                        fileBaru.write(hurufDepan + " " + nomor + " " + belakang + "\n");
                    }
                }
            }
            System.out.println("Berhasil membuat ");
            fileBaru.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            System.out.println("sudah ada filenya");
        }
    }
}
