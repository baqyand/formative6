package com.formative2;

import java.util.ArrayList;
import java.util.Random;

public class Formative23 {
    public static ArrayList<String> namaAsli = new ArrayList<>();
    public static ArrayList<String> namaAcak = new ArrayList<>();

    public static void main(String[] args) {
        String[] nama = allName();
        acak(nama);

        //looping untuk mengacak kalimat
        System.out.println("========= Acak kalimat ========");
        for (int i = 0; i < namaAsli.size(); i++) {
            System.out.println(nama[i] + " -------------- " + namaAcak.get(i));
        }

        //looping untuk mengembalikan kalimat
        System.out.println("\n\n========== Mengembalikan ==========");
        for (int i = 0; i < namaAcak.size(); i++) {
            System.out.println(namaAcak.get(i) + " --------------- " + kembali(i));
        }
    }

    //method untuk megnembalikan kata yang telah di acak
    public static String kembali(int i) {
        return namaAsli.get(i);
    }

    //method acak kalimat yang sudah di buat
    public static void acak(String[] nama) {
        //init String builder
        StringBuilder acakKalimat = new StringBuilder();
        for (var list : nama) {
            namaAsli.add(list);
            String[] kata = list.split(" ");
            String[] kataAcak = new String[kata.length];
            Random random = new Random();
            for (int i = 0; i < kata.length; i++) {
                char[] character = kata[i].toCharArray();
                for (int j = 0; j < character.length; j++) {
                    int a = random.nextInt(character.length);
                    char b = character[a];
                    character[a] = character[j];
                    character[j] = b;
                }
                kataAcak[i] = String.valueOf(character);
            }
            for (String s : kataAcak) {
                acakKalimat.append(s).append(" ");
            }
            namaAcak.add(String.valueOf(acakKalimat));
            acakKalimat.delete(0, acakKalimat.length());
        }
    }

    public static String[] allName() {
        return new String[]{
                "Bama Qyandija Deandra",
                "Fajar Mustakim",
                "Krisna Riyadi",
                "Candra Mukti Gimnastiar",
                "M sabil Fausta",
                "Azka Zaki Ramadan"
        };
    }
}
